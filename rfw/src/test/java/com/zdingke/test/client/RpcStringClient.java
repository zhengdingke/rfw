package com.zdingke.test.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.model.RpcDecoder;
import com.zdingke.rfw.model.RpcEncoder;

public class RpcStringClient {

    private static final Log log = LogFactory.getLog(RpcStringClient.class);
    static final boolean SSL = System.getProperty("ssl") != null;
    static final String HOST = System.getProperty("host", "127.0.0.1");
    static final int PORT = Integer.parseInt(System.getProperty("port", "8007"));
    ChannelFuture future;

    public void start() throws SSLException, InterruptedException {
        final SslContext sslCtx;
        if (SSL) {
            sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
        } else {
            sslCtx = null;
        }
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY, true).handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ChannelPipeline p = ch.pipeline();
                    if (sslCtx != null) {
                        p.addLast(sslCtx.newHandler(ch.alloc(), HOST, PORT));
                    }
                    p.addLast(new RpcEncoder(String.class));
                    p.addLast(new RpcDecoder(String.class));
                    p.addLast(new RpcStringClientHandler());
                }
            });

            future = b.connect(HOST, PORT).sync();
            future.channel().writeAndFlush("dingke");
            RpcStringClientHandler handler = (RpcStringClientHandler) future.channel().pipeline().last();
            log.info(handler.getResult());
        } finally {
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws SSLException, InterruptedException {
        RpcStringClient c = new RpcStringClient();
        c.start();
    }

}
