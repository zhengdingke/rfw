package com.zdingke.test.client;

/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class RpcStringClientHandler extends SimpleChannelInboundHandler<String> {
    final BlockingQueue<String> answer = new LinkedBlockingQueue<String>();
	
    public String getResult() {
		boolean interrupted = false;
        try {
            for (;;) {
                try {
                    return answer.take();
                } catch (InterruptedException ignore) {
                    interrupted = true;
                }
            }
        } finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
        System.out.println("channelactive");
        // RpcRequest request = new RpcRequest();
        // request.setMethodName("dingke");
        // ctx.writeAndFlush(request);
	}


	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		System.out.println(cause.getMessage());
		ctx.close();
	}

	@Override
    protected void messageReceived(ChannelHandlerContext ctx, String response)
			throws Exception {
        System.out.println("get the response" + response);
		answer.offer(response);
		ctx.close();
	}
}
