package com.zdingke.test.utils;

import java.io.IOException;
import java.net.InetAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;

import com.zdingke.rfw.utils.ZkUtil;

public class TestZkUtil {
    private static final Log LOG = LogFactory.getLog(TestZkUtil.class);

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
        LOG.info("DINGKE");

        InetAddress s = InetAddress.getLocalHost();
        System.out.println(s.getCanonicalHostName());
        ZkUtil util = new ZkUtil();

        String path2 = "/" + s.getCanonicalHostName() + "/" + "dingke3";
        util.getZooKeeper().create(path2, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
        String path3 = "/" + s.getCanonicalHostName() + "/" + "dingke4";
        util.getZooKeeper().create(path3, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
    }
}
