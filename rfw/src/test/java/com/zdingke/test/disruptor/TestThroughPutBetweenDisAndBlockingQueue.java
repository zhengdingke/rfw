package com.zdingke.test.disruptor;

import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.Test;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

public class TestThroughPutBetweenDisAndBlockingQueue {

    @SuppressWarnings("unchecked")
    @Test
    public void testDisruptor() throws InterruptedException {
        long start = System.currentTimeMillis();
        Executor executor = Executors.newCachedThreadPool();
        int bufferSize = 16384;// Construct the Disruptor
        Disruptor<LongEvent> disruptor = new Disruptor<>(LongEvent::new, bufferSize, executor);
        disruptor.handleEventsWith((event, sequence, endOfBatch) -> {
            System.out.println("consumer1: " + event.getValue());
        });

        disruptor.start();
        RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();

        ByteBuffer bb = ByteBuffer.allocate(8);
        for (long l = 0; l < 50000; l++) {
            bb.putLong(0, l);
            ringBuffer.publishEvent((event, sequence, buffer) -> event.setValue(buffer.getLong(0)), bb);
            Thread.sleep(1);
        }
        System.out.println("花费用时：" + (System.currentTimeMillis() - start));
    }

    @Test
    public void testLinkBlockingQueue() throws InterruptedException {
        long start = System.currentTimeMillis();
        LinkedBlockingQueue<Long> rqueue =  new LinkedBlockingQueue<Long>();
        LinkedBlockingQueue<Long> queuet1 =  new LinkedBlockingQueue<Long>();
        LinkedBlockingQueue<Long> queuet11 = new LinkedBlockingQueue<Long>();
        LinkedBlockingQueue<Long> queuet2 = new LinkedBlockingQueue<Long>();

        TestThread th1 = new TestThread(rqueue, queuet1);
        TestThread th11 = new TestThread(rqueue, queuet11);
        TestThread th2 = new TestThread(queuet1, queuet2);
        TestThread th22 = new TestThread(queuet11, queuet2);
        new Thread(th1).start();
        new Thread(th11).start();
        new Thread(th2).start();
        new Thread(th22).start();
        for (long l = 0; l < 50000; l++) {
            rqueue.add(l);
            System.out.println("add:" + l);
            Thread.sleep(1);
        }
        System.out.println("花费用时：" + (System.currentTimeMillis() - start));
    }

    public class TestThread implements Runnable {
        LinkedBlockingQueue<Long> queue = null;
        LinkedBlockingQueue<Long> queuet1 = null;
        public TestThread(LinkedBlockingQueue<Long> queue,LinkedBlockingQueue<Long> queuet1) {
            this.queue = queue;
            this.queuet1 = queuet1;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    long l = queue.take();
                    System.out.println(l);
                    queuet1.put(queue.take());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public class LongEvent {
        private long value;

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }
    }

}
