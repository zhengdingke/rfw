package com.zdingke.test.disruptor;

import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.junit.Test;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

public class TestDisruptor {

    @Test
    public void testParallel() throws InterruptedException {
        DisruptorTest event = new DisruptorTest();
        event.startParallel(null);
    }

    @Test
    public void testDependencies() throws InterruptedException {
        DisruptorTest event = new DisruptorTest();
        event.startDependencies(null);
    }


    public class LongEvent {
        private long value;

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }
    }

    public class DisruptorTest {

        @SuppressWarnings("unchecked")
        public void startParallel(String[] args) throws InterruptedException {
            // Executor that will be used to construct new threads for consumers
            Executor executor = Executors.newCachedThreadPool();
            // Specify the size of the ring buffer, must be power of 2.
            int bufferSize = 8;// Construct the Disruptor
            Disruptor<LongEvent> disruptor = new Disruptor<>(LongEvent::new, bufferSize, executor);
            // 可以使用lambda来注册一个EventHandler
            disruptor.handleEventsWith((event, sequence, endOfBatch) -> {
                System.out.println("consumer1: " + event.getValue());
                System.out.println("sequencer1:" + sequence);
            }, (event, sequence, endOfBatch) -> {
                System.out.println("consumer2: " + event.getValue());
                System.out.println("sequencer2:" + sequence);
            });

            // Start the Disruptor, starts all threads running
            disruptor.start();
            // Get the ring buffer from the Disruptor to be used for publishing.
            RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();

            ByteBuffer bb = ByteBuffer.allocate(8);
            for (long l = 0; true; l++) {
                bb.putLong(0, l);
                ringBuffer.publishEvent((event, sequence, buffer) -> event.setValue(buffer.getLong(0)), bb);
                Thread.sleep(1000);
            }
        }

        @SuppressWarnings("unchecked")
        public void startDependencies(String[] args) throws InterruptedException {
            // Executor that will be used to construct new threads for consumers
            Executor executor = Executors.newCachedThreadPool();
            // Specify the size of the ring buffer, must be power of 2.
            int bufferSize = 8;// Construct the Disruptor
            Disruptor<LongEvent> disruptor = new Disruptor<>(LongEvent::new, bufferSize, executor);
            // 可以使用lambda来注册一个EventHandler
            disruptor.handleEventsWith((event, sequence, endOfBatch) -> {
                System.out.println("consumer1: " + event.getValue());
                System.out.println("sequencer1:" + sequence);
            }).then((event, sequence, endOfBatch) -> {
                System.out.println("consumer2: " + event.getValue());
                System.out.println("sequencer2:" + sequence);
            });

            // Start the Disruptor, starts all threads running
            disruptor.start();
            // Get the ring buffer from the Disruptor to be used for publishing.
            RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();

            ByteBuffer bb = ByteBuffer.allocate(8);
            for (long l = 0; true; l++) {
                bb.putLong(0, l);
                ringBuffer.publishEvent((event, sequence, buffer) -> event.setValue(buffer.getLong(0)), bb);
                Thread.sleep(1000);
            }
        }

        @SuppressWarnings("unchecked")
        public void startProcessors(String[] args) throws InterruptedException {
            // Executor that will be used to construct new threads for consumers
            Executor executor = Executors.newCachedThreadPool();
            // Specify the size of the ring buffer, must be power of 2.
            int bufferSize = 8;// Construct the Disruptor
            Disruptor<LongEvent> disruptor = new Disruptor<>(LongEvent::new, bufferSize, executor);

            RingBuffer<LongEvent> ringBuffer = disruptor.getRingBuffer();

            ringBuffer.newBarrier();

            ByteBuffer bb = ByteBuffer.allocate(8);
            for (long l = 0; true; l++) {
                bb.putLong(0, l);
                ringBuffer.publishEvent((event, sequence, buffer) -> event.setValue(buffer.getLong(0)), bb);
                Thread.sleep(1000);
            }
        }
    }

}
