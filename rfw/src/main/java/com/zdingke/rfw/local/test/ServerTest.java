package com.zdingke.rfw.local.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.server.RpcServer;

public class ServerTest {

    private static final Log log = LogFactory.getLog(ServerTest.class);

    public static void main(String[] args) throws Exception {
        RpcServer.start();
    }
}
