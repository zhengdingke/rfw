package com.zdingke.rfw.local.test;

import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.service.IService;
import com.zdingke.rfw.service.client.Client;

public class ClientTest {

    private static final Log log = LogFactory.getLog(ClientTest.class);

    public static void main(String[] args) throws SSLException, InterruptedException {
        IService service = Client.getServiceByName(IService.class);
        String sayHello = service.sayHello("dingke service Test");
        log.info(sayHello);
    }
}
