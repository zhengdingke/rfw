package com.zdingke.rfw.service.client;

import java.lang.reflect.Proxy;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.client.RpcClient;
import com.zdingke.rfw.model.RpcRequest;

public class Client {

    private static final Log log = LogFactory.getLog(Client.class);


    @SuppressWarnings("unchecked")
    public static <T> T getServiceByName(final Class<T> serviceInterface) {
        return (T) Proxy.newProxyInstance(serviceInterface.getClassLoader(), new Class[] { serviceInterface }, (proxy, method, objs) -> {
            RpcRequest request = new RpcRequest();
            request.setRequestId(UUID.randomUUID().toString());
            request.setMethodName(method.getName());
            request.setParameterTypes(method.getParameterTypes());
            request.setServiceName(serviceInterface.getName());
            request.setParameters(objs);
            log.info(request.toString());
            return RpcClient.start(request);
        });
    }
}
