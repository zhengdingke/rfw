package com.zdingke.rfw.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;

public class Constants {
    private static final Log LOG = LogFactory.getLog(Constants.class);
    private static final String ZK_CONNECT_ADDR = "";
    private static final String ZK_SESSION_TIMEOUT = "";

    static {
        Configuration.addDefaultResource("rcluster-site.xml");
    }

    public static Configuration conf = new Configuration();
}
