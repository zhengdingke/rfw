package com.zdingke.rfw.common;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

public class Context {

	public static EventLoopGroup group = null;

	public static EventLoopGroup getGroup() {
		if(group==null){
			group = new NioEventLoopGroup();
		}
		return group;
	}
}
