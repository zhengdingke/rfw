package com.zdingke.rfw.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import javax.net.ssl.SSLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.client.handler.RpcClientHandler;
import com.zdingke.rfw.common.Context;
import com.zdingke.rfw.model.RpcDecoder;
import com.zdingke.rfw.model.RpcEncoder;
import com.zdingke.rfw.model.RpcRequest;
import com.zdingke.rfw.model.RpcResponse;

public class RpcClient {
    static final boolean SSL = System.getProperty("ssl") != null;
	static final String HOST = System.getProperty("host", "127.0.0.1");
	static final int PORT = Integer.parseInt(System.getProperty("port", "8007"));
    private static final Log LOG = LogFactory.getLog(RpcClient.class);


    public static String start(RpcRequest request) throws SSLException, InterruptedException {
        final SslContext sslCtx;
        if (SSL) {
            sslCtx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
        } else {
            sslCtx = null;
        }
		EventLoopGroup group = Context.getGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group)
			.channel(NioSocketChannel.class)
			.option(ChannelOption.TCP_NODELAY, true)
			.handler(new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel ch) throws Exception {
					ChannelPipeline p = ch.pipeline();
					if (sslCtx != null) {
						p.addLast(sslCtx.newHandler(ch.alloc(), HOST, PORT));
					}
					p.addLast(new RpcEncoder(RpcRequest.class));
					p.addLast(new RpcDecoder(RpcResponse.class));
					p.addLast(new RpcClientHandler());
				}
			});

            ChannelFuture future = b.connect(HOST, PORT).sync();
            future.awaitUninterruptibly().channel().writeAndFlush(request);
            RpcClientHandler handler = (RpcClientHandler) future.channel().pipeline().last();
            return (String)handler.getResult().getResult();
		} finally {
			group.shutdownGracefully();
		}
	}


}
