package com.zdingke.rfw.model;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import com.zdingke.rfw.utils.SerializationUtil;

public class RpcDecoder extends ByteToMessageDecoder{

    public Class<?> claz;

    public RpcDecoder(Class<?> claz) {
        this.claz = claz;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        if (in.readableBytes() < 4) {
            System.out.println("kkkkkk");
            return;
        }
        in.markReaderIndex();
        int dataLength = in.readInt();
        if (dataLength < 0) {
            ctx.close();
        }
        if (in.readableBytes() < dataLength) {
            in.resetReaderIndex();
        }
        byte[] data = new byte[dataLength];
        in.readBytes(data);
        Object obj = SerializationUtil.deserialize(data, this.claz);
        out.add(obj);
    }

    public static void main(String[] args) {

    }
}
