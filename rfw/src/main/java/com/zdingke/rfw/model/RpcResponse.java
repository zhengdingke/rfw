package com.zdingke.rfw.model;

public class RpcResponse {

    private String requestId;
    private Object result;

    public RpcResponse() {

    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}
