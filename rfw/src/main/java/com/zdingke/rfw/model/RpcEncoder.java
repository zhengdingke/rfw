package com.zdingke.rfw.model;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import com.zdingke.rfw.utils.SerializationUtil;

public class RpcEncoder extends MessageToByteEncoder {

    public Class<?> claz;

    public RpcEncoder(Class<?> claz) {
        this.claz = claz;
    }

    @Override
    protected void encode(ChannelHandlerContext arg0, Object in, ByteBuf out) {
        if (claz.isInstance(in)) {
            byte[] bytes = SerializationUtil.serialize(in);
            out.writeInt(bytes.length);
            out.writeBytes(bytes);
        }
    }
}
