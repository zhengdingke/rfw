package com.zdingke.rfw.server.handler;

/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.zdingke.rfw.model.RpcRequest;
import com.zdingke.rfw.model.RpcResponse;
import com.zdingke.rfw.service.HelloWorldService;

/**
 * Handler implementation for the echo server.
 */
@Sharable
public class RpcServerHandler extends SimpleChannelInboundHandler<RpcRequest> {

    private static final Log LOG = LogFactory.getLog(RpcServerHandler.class);
    private final ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, RpcRequest request) throws Exception {
        LOG.info("server receive message!!");
        TestCallable test = new TestCallable(ctx, request);
        exec.submit(test);
    }

    public class TestCallable implements Callable<Boolean> {
        private ChannelHandlerContext ctx;
        private RpcRequest request;

        public TestCallable(ChannelHandlerContext ctx, RpcRequest request) {
            this.ctx = ctx;
            this.request = request;
        }

        @Override
        public Boolean call() throws Exception {
            RpcResponse response = new RpcResponse();
            response.setResult(parseRequest(request));
            ctx.writeAndFlush(response);
            return true;
        }

        private String parseRequest(RpcRequest request) throws Exception {
            HelloWorldService service = new HelloWorldService();
            Method method = service.getClass().getMethod("sayHello", request.getParameterTypes());
            return (String) method.invoke(service, request.getParameters());
        }
    }

}
